export default {
  state: {
    videoId: ''
  },
  mutations: {
    setVideoId (state, data) {
      state.videoId = data
    }
  }
}
